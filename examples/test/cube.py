#! /usr/bin/env python

"""cube.py - plots a cube

  Copyright (C) 2009 Thomas J. Duck
  All rights reserved.

  Thomas J. Duck <tom.duck@dal.ca>
  Department of Physics and Atmospheric Science,
  Dalhousie University, Halifax, Nova Scotia, Canada, B3H 3J5

NOTICE

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Library General Public
  License as published by the Free Software Foundation; either
  version 2 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Library General Public License for more details.

  You should have received a copy of the GNU Library General Public
  License along with this library; if not, write to the
  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
  Boston, MA 02111-1307, USA.
"""

import gts
from enthought.mayavi import mlab
from math import radians

EPS = 2**(-51)
SCALE = 1.+EPS

s1 = gts.cube()
s2 = gts.cube()

s2.scale(1.,SCALE,SCALE)
s2.translate(0.7)

s3 = s1.union(s2)

s3.coarsen(30)

print '\n\n***',s3.Nedges

#s3.cleanup(1.e-9)

#for v in s3.vertices():
#    print "%.16e %.16e %.16e" %(v.x,v.y,v.z)

# Plot the surface
x,y,z,t = gts.get_mayavi_coords_and_triangles(s3)
mlab.triangular_mesh(x,y,z,t,color=(0.5,0.5,0.75))
mlab.triangular_mesh(x,y,z,t,color=(0,0,1),representation='fancymesh',
                     scale_factor=0.2)
mlab.show()
