#! /usr/bin/env python

"""die.py - plots a die

  Copyright (C) 2009 Thomas J. Duck
  All rights reserved.

  Thomas J. Duck <tom.duck@dal.ca>
  Department of Physics and Atmospheric Science,
  Dalhousie University, Halifax, Nova Scotia, Canada, B3H 3J5

NOTICE

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Library General Public
  License as published by the Free Software Foundation; either
  version 2 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Library General Public License for more details.

  You should have received a copy of the GNU Library General Public
  License along with this library; if not, write to the
  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
  Boston, MA 02111-1307, USA.
"""

import gts
from enthought.mayavi import mlab
import sys

s1 = gts.cube()

def add_dot(s1,x,y,z):

    s2 = gts.sphere(2)
    s2.scale(0.2,0.2,0.2)
    s2.translate(x,y,z)

    return s1.difference(s2)

## 1
#s1 = add_dot(s1,0,0,1)
#
## 2
#s1 = add_dot(s1,1,0,0.5)
#s1 = add_dot(s1,1,0,-0.5)
#
## 3
#s1 = add_dot(s1,0,1,0.65)
#s1 = add_dot(s1,0,1,-0.65)
#s1 = add_dot(s1,0,1,0)

s1.tessellate()

# 6
#s1 = add_dot(s1,-0.5,-1,0.65)
#s1 = add_dot(s1,-0.5,-1.0,0.0)
#s1 = add_dot(s1,-0.5,-1.0,-0.65)
#
#s1 = add_dot(s1,0.5,-1,0.65)
#s1 = add_dot(s1,0.5,-1,0)
#s1 = add_dot(s1,0.5,-1,-0.65)

# Plot the surfaces
x,y,z,t = gts.get_coords_and_face_indices(s1,True)

mlab.triangular_mesh(x,y,z,t,color=(0.9,0.9,0.9),representation='fancymesh')

sys.stdout.flush()

#x,y,z,t = gts.get_coords_and_face_indices(dot,True)
#mlab.triangular_mesh(x,y,z,t,color=(0,0,0))


mlab.show()
